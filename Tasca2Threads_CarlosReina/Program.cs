﻿using System;
using System.Threading;

namespace Tasca2Threads_CarlosReina
{

    internal class Nevera
    {
        private int _cerveses;

        public Nevera(int cerveses)
        {
            _cerveses = cerveses;
        }

        public void OmplirNevera(string nom)
        {
            if (_cerveses == 9)
            {
                Console.WriteLine(nom + " no pot omplir la nevera perquè ja esta plena.");
            }
            else
            {
                Random r = new Random();
                _cerveses += r.Next(1, 6);
                if (_cerveses > 9)
                {
                    _cerveses = 9;
                }
                Console.WriteLine(nom + " ha omplert la nevera, ara té " + _cerveses + " cerveses");
            }
        }


        public void BeureCervesa(string nom)
        {
            if (_cerveses == 0)
            {
                Console.WriteLine(nom + " no pot beure perquè no hi han cerveses a la nevera.");
            }
            else
            {
                Random rnd = new Random();
                _cerveses -= rnd.Next(1, 6);
                if (_cerveses < 0)
                {
                    _cerveses = 0;
                }
                Console.WriteLine(nom + " ha begut cerveses de la nevera, queden " + _cerveses);
            }
        }
    }

    internal class Program
    {

        static void Main()
        {
            Console.WriteLine("Exercici 1 (frases):");
            Exercici1();
            Console.WriteLine();
            Console.WriteLine("Exercici 2 (Nevera):");
            Exercici2();
        }

        public static void Exercici1()
        {
            string frase1 = "Una vegada hi havia un gat";
            string frase2 = "En un lugar de la mancha";
            string frase3 = "Once upon a time in the west";
            Thread t1 = new Thread(() => EscriureFrase(frase1, 1000));
            Thread t2 = new Thread(() =>
            {
                t1.Join();
                EscriureFrase(frase2, 1000);
            });
            Thread t3 = new Thread(() =>
            {
                t2.Join();
                EscriureFrase(frase3, 1000);
            });
            t1.Start();
            t2.Start();
            t3.Start();
            t3.Join();
            Console.WriteLine();
            Console.Write("Presiona enter per continuar...");
            Console.ReadLine();
        }

        public static void EscriureFrase(string frase, int segons)
        {
            string[] paraulas = frase.Split(' ');
            foreach (string paraula in paraulas)
            {
                Console.Write(paraula);
                Thread.Sleep(segons);
                Console.Write(' '); // espai entre paraules
            }
            Console.Write("\r\n");
        }

        public static void Exercici2()
        {
            Nevera nevera = new Nevera(6);

            Thread t1 = new Thread(() => nevera.OmplirNevera("Anitta"));
            Thread t2 = new Thread(() => nevera.BeureCervesa("Bad Bunny"));
            Thread t3 = new Thread(() => nevera.BeureCervesa("Lil Nas X"));
            Thread t4 = new Thread(() => nevera.BeureCervesa("Manuel Turizo"));

            t1.Start();

            t1.Join();
            t2.Start();

            t2.Join();
            t3.Start();

            t3.Join();
            t4.Start();
        }
    }
}
